# PROTOCOL: Inference of Episodic Changes in Natural Selection Acting on Protein Coding Sequences via CODEML

### What is this repsotiory for? ###

This is a public repsotiprty for a unit in *Current Protocols in Bioinformatics* that provides protocols for using the CODEML program from the PAML to make inferences about episodic natural selection in protein-coding sequences. The unit is comorised of 3 basic protocols and 5 support protocols, which are based on real data exmaples. The unit also provides an example workflow which, through automation, is readily extendable to a larger-scale evolutionary survey. The unit concludes with a discussion of best practices based on planning a series of robustness and reliability analyses.

### It's role as an "alternative PAML lab" in the Workshop on Molecular Evolution at MBL ###

If you have some experience with codon models, and want to try out a tutorial for more advanced materials then use the link below to download an archive for a complete different set of PAML activities. This tutorial focuses on detecting episodic protein evolution via Branch-Site Model A. The tutorial also includes activities about (i) detecting MLE instabilities, (ii) carrying out robustness analyses, and (iii) use of smoothed bootstrap aggregation (SBA). The protocols for each activity are presented in Protocols in Bioinformatics UNIT 6.15. The included PDF file for UNIT 6.16 also presents recommendations for "best practices" when carrying out a large-scale evolutionary survey for episodic adaptive evolution by using PAML.

### Contents ###

1. Book Chapter (PDF)

2. Seqeunce data and tree files for the Ceacam dataset

3. Seqeunce data and tree files for the NR1D1 dataset

4. Archive (zip) of samples of annotated output the codeml program

### How to cite the article ###

Bielawski, J.P., Baker, J.L. and Mingrone, J. 2016. Inference of episodic changes in natural selection acting on protein coding sequences via CODEML. Curr. Protoc. Bioinform. 54:6.15.1-6.15.32.
doi: 10.1002/cpbi.2

### Article Abstract ###

This unit provides protocols for using the CODEML program from the PAML package to make inferences about episodic natural selection in protein-coding sequences. The protocols cover inference tasks such as maximum likelihood estimation of selection intensity, testing the hypothesis of episodic positive selection, and identifying sites with a history of episodic evolution. We provide protocols for using the rich set of models implemented in CODEML to assess robustness, and for using bootstrapping to assess if the requirements for reliable statistical inference have been met. An example dataset is used to illustrate how the protocols are used with real protein-coding sequences. The workflow of this design, through automation, is readily extendable to a larger-scale evolutionary survey, 2016, John Wiley & Sons, Inc.